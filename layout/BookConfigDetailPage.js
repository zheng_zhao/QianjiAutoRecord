
/*
 * @Date         : 2021-02-17 12:57:14
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 19:49:10
 * @FilePath     : \QianjiAutoRecord\layout\BookConfigDetailPage.js
 */

let BookConfigDetailPage = function (auto_record, idx) {
    let book_dict = auto_record.book_config[idx]
    let book_name = book_dict["book_name"]
    let income_list = book_dict["收入"]
    let outcome_list = book_dict["支出"]

    ui.layout(
        <frame>
            <vertical>
                <text text="账本详细设置" textSize="28sp" textColor="#000000" gravity="center_horizontal"/>
                <text text="下方中间为账本名称，左侧为支出，右侧为收入。修改类别时，输入完成需要点击“改”按钮。点击保存才可生效，中途退出不保存"
                      gravity="center_horizontal"/>
                <card w="*" h="auto" foreground="?selectableItemBackground" cardCornerRadius="32px">
                    <vertical>
                        <horizontal w="*">
                            <button id="cancel" text="取消" w="100px" layout_weight="1"/> 
                            <input id="book_name" textSize="16sp" textColor="#000000" gravity="center" layout_weight="2"/>
                            <button id="save" text="保存" w="100px" layout_weight="1"/>
                        </horizontal>
                        
                        <horizontal>
                            <text text="支出" textColor="#000000" textSize="16sp" gravity="center" layout_weight="1" />
                            <text text="收入" textColor="#000000" textSize="16sp" gravity="center" layout_weight="1" />
                        </horizontal>

                        <horizontal>
                            <input id="outcome_new" hint="支出类别" text="" textColor="#000000" textSize="16sp" gravity="center" layout_weight="2" />
                            <button id="outcome_confirm"  text="添加" w="100px" layout_weight="1"/>
                            <input id="income_new" hint="收入类别" text="" textColor="#000000" textSize="16sp" gravity="center" layout_weight="2" />
                            <button id="income_confirm" text="添加" w="100px" layout_weight="1"/>
                        </horizontal>
                    </vertical>
                </card>
                
                <frame w="*">
                    <horizontal>
                        <frame w="*" layout_weight="1">
                        <list id="outcome_list">
                            <vertical w="*">
                                <card  w="*" h="auto" foreground="?selectableItemBackground"
                                    cardCornerRadius="32px">
                                    <vertical w="*">
                                        <horizontal>
                                            <input id="cate_name" text="{{this}}" textSize="16sp" layout_gravity="center" layout_weight="1"/>

                                            <vertical w="200px">
                                                <horizontal w="auto">
                                                    <button id="upItem" text="上" textSize="12sp" layout_weight="1"/>
                                                    <button id="downItem" text="下" textSize="12sp" layout_weight="1"/>
                                                </horizontal>
                                                <horizontal w="auto">
                                                    <button id="modifyItem" text="改" textSize="12sp" layout_weight="1"/>
                                                    <button id="deleteItem" text="删" textSize="12sp" layout_weight="1"/>
                                                </horizontal>
                                                
                                            </vertical>
                                        </horizontal>
                                    </vertical>
                                </card>
                            </vertical>
                        </list>
                        </frame>
                        
                        <frame w="*" layout_weight="1">
                        <list id="income_list">
                            <vertical w="*">
                                <card w="*" h="auto" foreground="?selectableItemBackground"
                                    cardCornerRadius="32px">
                                    <vertical w="*">
                                        <horizontal>
                                            <input id="cate_name" text="{{this}}" textSize="16sp" layout_gravity="center" layout_weight="1"/>

                                            <vertical w="200px">
                                                <horizontal w="auto">
                                                    <button id="upItem" text="上" textSize="12sp" layout_weight="1"/>
                                                    <button id="downItem" text="下" textSize="12sp" layout_weight="1"/>
                                                </horizontal>
                                                <horizontal w="auto">
                                                    <button id="modifyItem" text="改" textSize="12sp" layout_weight="1"/>
                                                    <button id="deleteItem" text="删" textSize="12sp" layout_weight="1"/>
                                                </horizontal>
                                            </vertical>
                                        </horizontal>
                                    </vertical>
                                </card>
                            </vertical>
                        </list>
                        </frame>
                    </horizontal>
                </frame>
            </vertical>
        </frame>
    )

    // 绑定数据
    ui.book_name.setText(book_name)
    ui.outcome_list.setDataSource(outcome_list)
    ui.income_list.setDataSource(income_list)

    ui.save.on("click", function() {     // 保存
        book_name = ui.book_name.getText()
        book_name = book_name.toString()

        let flag = true
        for(var i=0; i<auto_record.book_config.length; ++i) {
            if(i != idx && book_name == auto_record.book_config[i]["book_name"]) {
                flag = false
                alert("不能与已有账本重名！！！")
            }
        }
        if(flag) {
            book_dict["book_name"] = book_name
            book_dict["支出"] = outcome_list
            book_dict["收入"] = income_list
            auto_record.book_config[idx] = book_dict
            auto_record.SaveBookConfig(auto_record)
            auto_record.BookConfigPage(auto_record)
        }
    })
    ui.cancel.on("click", function() {  // 取消
        auto_record.BookConfigPage(auto_record)
    })

    ui.outcome_confirm.on("click", function() {
        let cate_name = ui.outcome_new.getText()
        cate_name = cate_name.toString()
        if(cate_name == "") {
            toast("类别不能为空！！！")
        } else {
            outcome_list.push(cate_name)
        }
        ui.outcome_new.setText("")
    })
    ui.income_confirm.on("click", function() { 
        let cate_name = ui.income_new.getText()
        cate_name = cate_name.toString()
        if(cate_name == "") {
            toast("类别不能为空！！！")
        } else {
            income_list.push(cate_name)
        }
        ui.income_new.setText("")
    })

    ui.outcome_list.on("item_bind", function(itemView, itemHolder){
        itemView.modifyItem.on("click", function () {
            let name = itemView.cate_name.getText()
            name = name.toString()
            outcome_list[itemHolder.position] = name
        })
        itemView.deleteItem.on("click", function(){
            outcome_list.splice(itemHolder.position, 1);
        })
        itemView.upItem.on("click", function(){
            let idx = itemHolder.position
            if(idx > 0) {
                let tmp = outcome_list[idx - 1]
                outcome_list[idx - 1] = outcome_list[idx]
                outcome_list[idx] = tmp
            }
        });
        itemView.downItem.on("click", function(){
            let idx = itemHolder.position
            if(idx < outcome_list.length - 1) {
                let tmp = outcome_list[idx + 1]
                outcome_list[idx + 1] = outcome_list[idx]
                outcome_list[idx] = tmp
            }
        });
    })

    ui.income_list.on("item_bind", function(itemView, itemHolder){
        itemView.modifyItem.on("click", function () {
            let name = itemView.cate_name.getText()
            name = name.toString()
            income_list[itemHolder.position] = name
        })
        itemView.deleteItem.on("click", function(){
            income_list.splice(itemHolder.position, 1);
        })
        itemView.upItem.on("click", function(){
            let idx = itemHolder.position
            if(idx > 0) {
                let tmp = income_list[idx - 1]
                income_list[idx - 1] = income_list[idx]
                income_list[idx] = tmp
            }
        });
        itemView.downItem.on("click", function(){
            let idx = itemHolder.position
            if(idx < income_list.length - 1) {
                let tmp = income_list[idx + 1]
                income_list[idx + 1] = income_list[idx]
                income_list[idx] = tmp
            }
        });
    })
}

module.exports = BookConfigDetailPage