
/*
 * @Date         : 2021-02-17 12:45:08
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-17 12:50:54
 * @FilePath     : \QianjiAutoRecord\layout\AccountConfigPage.js
 */

let AccountConfigPage = function (auto_record) {
    ui.layout(
        <frame>
            <vertical>
                <text text="账户配置" textSize="28sp" textColor="#000000" gravity="center_horizontal"/>
                <text text="返回键会退出程序！！！请点击取消返回主页面。左侧为付款应用显示的账户名称，右侧为钱迹中对应的账户名。输入后点击新增，拉到列表末尾即可看到。修改已有名称后必须点击“改”按钮。全部修改完后点击保存才会应用，中途退出不保存！"
                      gravity="center_horizontal"/>
                <card w="*" h="auto" foreground="?selectableItemBackground" cardCornerRadius="32px">
                    <vertical>
                        <frame>
                            <button id="cancel" text="取消" w="auto"/>   
                            <button id="save" text="保存" w="auto" layout_gravity="right"/>
                        </frame>
                        
                        <horizontal>
                            <text id="from_account" text="页面显示账户名"
                                textColor="#000000" textSize="20sp"
                                layout_gravity="center" layout_weight="1" />
                            <text id="to_account" text="对应钱迹账户名"
                                textColor="#000000" textSize="20sp"
                                layout_gravity="center" layout_weight="1" />
                        </horizontal>

                        <horizontal>
                            <input layout_weight="1" id="from_account_new" 
                                    textSize="12sp" textColor="#000000" layout_gravity="center"/>
                            <input layout_weight="1" id="to_account_new"
                                    textSize="12sp" textColor="#000000" layout_gravity="center"/>
                        </horizontal>
                        <button id="new" text="新增" layout_gravity="right"/>
                    </vertical>
                </card>
                
                <frame w="*">
                    <list id="list" w="*">
                        <vertical w="*">
                            <card id="item_card" w="*" h="auto" foreground="?selectableItemBackground"
                                cardCornerRadius="32px">
                            
                                <vertical w="*">
                                    <horizontal>
                                        <input id="from_account" text="{{from_account}}" textSize="16sp"  layout_gravity="center" layout_weight="1"/>
                                        <text text=": " textSize="20sp" textColor="#000000" layout_gravity="center_vertical"/>
                                        <input id="to_account" text="{{to_account}}" textSize="16sp" layout_gravity="center" layout_weight="1"/>
                                        <vertical w="200px">
                                            <horizontal w="auto">
                                                <button id="upItem" text="上" textSize="12sp" layout_weight="1"/>
                                                <button id="downItem" text="下" textSize="12sp" layout_weight="1"/>
                                            </horizontal>
                                            <horizontal w="auto">
                                                <button id="modifyItem" text="改" textSize="12sp" layout_weight="1"/>
                                                <button id="deleteItem" text="删" textSize="12sp" layout_weight="1"/>
                                            </horizontal>
                                        </vertical>
                                    </horizontal>
                                </vertical>
                            </card>
                        </vertical>
                    </list>
                </frame>
            </vertical>
        </frame>
    )

    // 绑定数据
    ui.list.setDataSource(auto_record.account_config);

    ui.new.on("click", function() {     // 新建账户映射
        let from_account = ui.from_account_new.getText()
        let to_account = ui.to_account_new.getText()
        from_account = from_account.toString()
        to_account = to_account.toString()
        from_account = from_account.replace("（", "(").replace("）", ")")
        to_account = to_account.replace("（", "(").replace("）", ")")

        if(from_account == "" || to_account == "") {
            toast("账户不能为空！！！")
        } else {
            let construct_data = {"from_account": from_account, "to_account": to_account}
            auto_record.account_config.push(construct_data)
            ui.from_account_new.setText("")
            ui.to_account_new.setText("")
        }
    })

    ui.save.on("click", function() {     // 保存
        auto_record.saveAccountConfig()
        auto_record.MainPage(auto_record)
    })
    ui.cancel.on("click", function() {  // 取消
        auto_record.account_config = auto_record.storage.get("AccountsConfig", [])
        auto_record.MainPage(auto_record)
    })

    ui.list.on("item_bind", function(itemView, itemHolder){     // 移动，删除数据
        itemView.modifyItem.on("click", function () {
            let from = itemView.from_account.getText()
            let to = itemView.to_account.getText()
            from = from.toString()
            to = to.toString()
            auto_record.account_config[itemHolder.position]["from_account"] = from
            auto_record.account_config[itemHolder.position]["to_account"] = to
        })
        itemView.deleteItem.on("click", function(){
            let item = itemHolder.item;
            auto_record.account_config.splice(itemHolder.position, 1);
        })
        itemView.upItem.on("click", function(){
            let idx = itemHolder.position
            if(idx > 0) {
                let tmp = auto_record.account_config[idx - 1]
                auto_record.account_config[idx - 1] = auto_record.account_config[idx]
                auto_record.account_config[idx] = tmp
            }
        });
        itemView.downItem.on("click", function(){
            let idx = itemHolder.position
            if(idx < auto_record.account_config.length - 1) {
                let tmp = auto_record.account_config[idx + 1]
                auto_record.account_config[idx + 1] = auto_record.account_config[idx]
                auto_record.account_config[idx] = tmp
            }
        });
    })
}

module.exports = AccountConfigPage