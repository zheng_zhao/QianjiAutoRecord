
/*
 * @Date         : 2021-01-23 23:57:21
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-17 13:38:19
 * @FilePath     : \QianjiAutoRecord\layout\MiniFloaty.js
 */

// const myUtil = require("../utils/Util.js")

let _debugInfo = typeof debugInfo === 'undefined' ? (v) => console.verbose(v) : debugInfo
let _errorInfo = typeof errorInfo === 'undefined' ? (v) => console.error(v) : errorInfo

let MiniFloaty = function () {
    this.floatyWindow = null
    this.floatyInitStatus = false
    this.floatyLock = null
    this.floatyCondition = null

    this.showLog = true
    this.debugInfo = function (content) {
      this.showLog && _debugInfo(content)
    }
}

MiniFloaty.prototype.init = function (analyze_param, config_param) {
    if (this.floatyInitStatus) {
        return true
    }

    this.floatyLock = threads.lock()
    this.floatyCondition = this.floatyLock.newCondition()
    let _this = this
    let W = device.width
    threads.start(function () {
        // 延迟初始化，避免死机
        sleep(200)
        _this.floatyLock.lock()
        try {
        if (_this.floatyInitStatus) {
            return true
        }
        _this.floatyWindow = floaty.rawWindow(
            <frame id="main_frame" w="100px" h="100px" alpha="0.5">
                <img id="circle_bg" src="#559AF9" w="*" h="*" circle="true"/>
                <img id="close_btn" src="@drawable/ic_close_black_48dp" w="*" h="*" marginLeft="1px" marginTop="3px" visibility="gone"/>
                <text id="word" text="自" textColor="#000000" textSize="20sp" marginLeft="1px" marginTop="-4px" gravity="center"/>
            </frame>
        )

        ui.run(function () { 
            _this.floatyWindow.setPosition(W - 100, 100)
            _this.floatyWindow.setTouchable(true)
        })

        // 保持开启
        setInterval( () => {}, 1000)
        // 点击按钮
        _this.floatyWindow.circle_bg.on("click", () => {
            _this.floatyWindow.word.attr("visibility", "gone")
            _this.floatyWindow.close_btn.attr("visibility", "visible")
            _this.floatyWindow.circle_bg.attr("src", "#FF0000")
            setTimeout( () => {
                try {
                    if(_this.floatyWindow.circle_bg) _this.floatyWindow.circle_bg.attr("src", "#559AF9")
                _this.floatyWindow.word.attr("visibility", "visible")
                _this.floatyWindow.close_btn.attr("visibility", "gone")
                } catch (e) {
                    console.log(e)
                }
                
            }, 10000)
        })
        // 关闭按钮
        _this.floatyWindow.close_btn.on("click", () => { _this.close() })
        
        _this.floatyInitStatus = true
        } catch (e) {
        _errorInfo('悬浮窗初始化失败' + e)
        _this.floatyInitStatus = false
        } finally {
        _this.floatyCondition.signalAll()
        _this.floatyLock.unlock()
        }
    })
    this.floatyLock.lock()
    try {
        if (this.floatyInitStatus === false) {
        this.debugInfo('等待悬浮窗初始化')
        this.floatyCondition.await()
        }
    } finally {
        this.floatyLock.unlock()
    }
    this.debugInfo('悬浮窗初始化' + (this.floatyInitStatus ? '成功' : '失败'))
    return this.floatyInitStatus
}

MiniFloaty.prototype.disAppear = function () {
    if (this.floatyInitStatus) {
        this.floatyLock.lock()
        try {
            if (this.floatyWindow !== null) {
                this.second = -1
                this.cancel_flag = true;
                this.floatyWindow.close()
                this.floatyWindow = null
            }
            this.floatyInitStatus = false
        } finally {
            this.floatyLock.unlock()
        }
    }
}

MiniFloaty.prototype.close = function () {
    this.disAppear()
    myUtil.stop()
}

module.exports = new MiniFloaty()