
/*
 * @Date         : 2021-02-17 12:51:02
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-17 13:44:33
 * @FilePath     : \QianjiAutoRecord\layout\BookConfigPage.js
 */

// const myUtil = require("../utils/Util.js")

let BookConfigPage = function(auto_record) {
    ui.layout(
        <frame>
            <vertical>
                <text text="账本配置" textSize="28sp" textColor="#000000" gravity="center_horizontal"/>
                <text text="下方按钮为模板，点击创建对应账本，需要手动启用。点击已创建列表中的账本名，进入详细配置页。"
                      gravity="center_horizontal"/>
                <frame>
                    <button id="cancel" text="取消" w="auto"/>   
                    <button id="save" text="保存" w="auto" layout_gravity="right"/>
                </frame>
                <card id="item_card" w="*" h="auto" foreground="?selectableItemBackground" cardCornerRadius="32px">
                    <vertical>
                        <horizontal>
                            <button id="book_template_0" text="日常账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_1" text="旅行账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_2" text="装修账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_3" text="结婚账本" layout_weight="1" gravity="center"/>
                        </horizontal>
                        <horizontal>
                            <button id="book_template_4" text="人情账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_5" text="出差账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_6" text="公司账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_7" text="店铺账本" layout_weight="1" gravity="center"/>
                        </horizontal>
                        <horizontal>
                            <button id="book_template_8" text="汽车账本" layout_weight="1" gravity="center"/>
                            <button id="book_template_white" text="空白账本" layout_weight="3" gravity="center"/>
                        </horizontal>
                        
                    </vertical>
                </card>

                <frame w="*">
                    <list id="list" w="*">
                        <vertical w="*">
                            <card id="item_card" w="*" h="auto" foreground="?selectableItemBackground"
                                cardCornerRadius="32px">
                                <horizontal>
                                    <button id="book_name" text="{{book_name}}" textSize="16sp" layout_weight="1" gravity="center" layout_gravity="center_vertical"/>
                                    <checkbox id="enable" text="启用" checked="{{enable}}" gravity="center_vertical" layout_gravity="center_vertical"/>
                                    <button id="deleteItem" text="删除"/>
                                </horizontal>
                            </card>
                        </vertical>
                    </list>
                </frame>
            </vertical>
        </frame>
    )

    // 绑定数据
    ui.list.setDataSource(auto_record.book_config)

    ui.save.on("click", function() {     // 保存
        var i = 0
        for(i=0; i<auto_record.book_config.length; ++i) {
            if(auto_record.book_config[i]["enable"] == true) break
        }
        if(i != auto_record.book_config.length) {
            auto_record.SaveBookConfig()
            auto_record.MainPage(auto_record)
        } else {
            alert("至少启用一个账本")
        }
        
    })
    ui.cancel.on("click", function() {  // 取消
        auto_record.book_config = auto_record.storage.get("BooksConfig", [])
        auto_record.MainPage(auto_record)
    })
    ui.book_template_0.on("click", function () { addBook(auto_record.default_books[0]) })
    ui.book_template_1.on("click", function () { addBook(auto_record.default_books[1]) })
    ui.book_template_2.on("click", function () { addBook(auto_record.default_books[2]) })
    ui.book_template_3.on("click", function () { addBook(auto_record.default_books[3]) })
    ui.book_template_4.on("click", function () { addBook(auto_record.default_books[4]) })
    ui.book_template_5.on("click", function () { addBook(auto_record.default_books[5]) })
    ui.book_template_6.on("click", function () { addBook(auto_record.default_books[6]) })
    ui.book_template_7.on("click", function () { addBook(auto_record.default_books[7]) })
    ui.book_template_8.on("click", function () { addBook(auto_record.default_books[8]) })
    ui.book_template_white.on("click", function () { addBook({"book_name": "空白账本", "enable": false, "收入": [], "支出": [] }) })

    ui.list.on("item_bind", function(itemView, itemHolder){
        itemView.deleteItem.on("click", function(){
            if(auto_record.book_config.length == 1) {
                alert("至少保留一个账本")
            } else {
                auto_record.book_config.splice(itemHolder.position, 1);
            }
        })
        itemView.book_name.on("click", function(){
            auto_record.BookConfigDetailPage(auto_record, itemHolder.position)
        })
        itemView.enable.on("check", (checked) => {
            if(checked) auto_record.book_config[itemHolder.position]["enable"] = true
            else        auto_record.book_config[itemHolder.position]["enable"] = false
        })
    })

    function addBook(book_dict) {
        auto_record.book_config.push(myUtil.copyDict(book_dict))
        let book_name = book_dict["book_name"]

        while(true) {
            var i;
            for(i=0; i<auto_record.book_config.length - 1; ++i) {
                if(book_name == auto_record.book_config[i]["book_name"]) {
                    book_name = book_name + "-新"
                    break
                }
            }
            if(i == auto_record.book_config.length - 1) {
                auto_record.book_config[i]["book_name"] = book_name
                break
            }
        }
        
    }
}

module.exports = BookConfigPage