/*
 * @Date         : 2021-01-10 12:11:27
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-16 23:27:13
 * @FilePath     : \QianjiAutoRecord\utils\Util.js
 */

let _debugInfo = typeof debugInfo === 'undefined' ? (v) => console.verbose(v) : debugInfo
let _errorInfo = typeof errorInfo === 'undefined' ? (v) => console.error(v) : errorInfo

let Util = function () {
    this.showLog = false
    this.debugInfo = function (content) {
      this.showLog && _debugInfo(content)
    }
}

/**
 * @description: 获取当前页面的名称
 * @param {Map} page_dict: 已知页面字典
 * @param {ArrayList} package_list: 已知页面的包名集合
 * @param {Map} activity_page_name_dict: activity 对应的页面名称列表
 * @param {Map} default_page: 包名对应的页面名称字典
 * @return {String}: 页面名称
 */    
Util.prototype.currentActivity2PageName = function(page_dict, package_list, activity_page_name_dict, default_page) {
    let curr_package = currentPackage()
    let curr_activity = currentActivity()
    let page_name = "undefined"

    // 根据包名确定默认页面
    if(this.inList(curr_package, package_list)) {
        page_name = default_page[curr_package]
        sleep(100)
    } else {
        // 不在目标页面，检测间隔 500ms
        sleep(500)
    }
    // 根据 activity 和 text_list 或 id_list 确定具体页面
    let page_name_list = activity_page_name_dict[curr_activity]
    if(page_name_list != null) {
        for(var i=0; i<page_name_list.length; ++i) {
            var target_page_name = page_name_list[i]
            var text_list = page_dict[target_page_name]["text_list"]
            var id_list = page_dict[target_page_name]["id_list"]
            var desc_list = page_dict[target_page_name]["desc_list"]
            
            if(curr_package == page_dict[target_page_name]["package_name"]) {
                if(text_list && id_list && desc_list) {
                    if(this.hasTextElement(text_list) &&
                    this.hasIdElement(id_list) && 
                    this.hasDescElement(desc_list) ){
                        page_name = target_page_name
                        break
                    }
                } else if(text_list && id_list) {
                    if(this.hasTextElement(text_list) && this.hasIdElement(id_list)) {
                        page_name = target_page_name
                        break
                    }
                } else if(text_list && desc_list) {
                    if(this.hasTextElement(text_list) && this.hasDescElement(desc_list)){
                        page_name = target_page_name
                        break
                    }
                } else if(id_list && desc_list) {
                    if(this.hasIdElement(id_list) && this.hasDescElement(desc_list)) {
                        page_name = target_page_name
                        break
                    }
                } else if(id_list) {
                    if(this.hasIdElement(id_list)) {
                        page_name = target_page_name
                        break
                    }
                } else if(text_list) {
                    if(this.hasTextElement(text_list)) {
                        page_name = target_page_name
                        break
                    }
                } else if(desc_list) {
                    if(this.hasDescElement(desc_list)) {
                        page_name = target_page_name
                        break
                    }
                } else {
                    page_name = target_page_name
                    break
                }
            }
        }
        // 在目标，检测间隔 200ms
        sleep(100)
    } else {
        // 不在目标 activity，检测间隔 300ms
        sleep(200)
    }
    return page_name;
}

/**
 * @description: 检测给定页面是否在给定页面列表中
 * @param {string} page_name: 页面名称
 * @param {ArrayList} page_list: 页面列表
 * @return {Boolean}
 */
Util.prototype.inPageList = function(page_name, page_list) {
    for(var i=0; i<page_list.length; ++i) {
        if(page_name == page_list[i])
            return true
        if(page_name && page_list[i] && 
           page_name.length == page_list[i].length + 1 && 
           page_name.startsWith(page_list[i]))
            return true
    }
    return false
}

/**
 * @description: 查询控件的 给定层数父节点的给定序列孩子控件
 * @param {*} widget: 给定控件
 * @param {Integer} parent_depth: 追溯父节点层数
 * @param {ArrayList} child_list: 孩子节点序列
 * @return {widget}: 目标控件
 */
Util.prototype.widgetByParentChild = function(widget, parent_depth, child_list) {
    let parent_widget = widget
    for(var i=0; i<parent_depth; ++i) {
        if(parent_widget) parent_widget = parent_widget.parent()
        else              break
    }
    if(parent_widget) {
        for(var i=0; i<child_list.length; ++i) {
            if(parent_widget.children().length <= child_list[i]) return null
            if(parent_widget) parent_widget = parent_widget.child(child_list[i])
            else              break
        }
    }
    return parent_widget
}

/**
 * @description: 在页面记录中向前回溯，检查目标页面列表中，哪个页面最近出现
 * @param {ArrayList} target_page_list: 目标页面列表
 * @param {ArrayList} page_record_list: 页面记录
 * @param {Integer} curr_idx: 当前页面指针
 * @return {String}: 最近出现的页面
 */
Util.prototype.findPreviewFirstInPageList = function(target_page_list, page_record_list, curr_idx) {
    let pre_page = null
    for(var i=1; i<page_record_list.length; ++i) {
        pre_page = page_record_list[(page_record_list.length + curr_idx - i) % page_record_list.length]
        if(this.inPageList(pre_page, target_page_list))
            break
    }
    return pre_page
}
/**
 * @description: 检查当前页面是否有列表中的 text
 * @param {ArrayList} text_list: text 列表
 * @return {Boolean}
 */    
Util.prototype.hasTextElement = function(text_list) {
    for(var i=0; i<text_list.length; ++i) {
        if(text(text_list[i]).findOnce() == null)
            return false
    }
    return true
},

/**
 * @description: 检查当前页面是否有列表中的 id
 * @param {ArrayList} id_list: id 列表
 * @return {Boolean}
 */
Util.prototype.hasIdElement = function(id_list) {
    for(var i=0; i<id_list.length; ++i) {
        if(id(id_list[i]).findOnce() == null)
            return false
    }
    return true
}

/**
 * @description: 检查当前页面是否有列表中的 desc
 * @param {ArrayList} desc_list
 * @return {Boolean}
 */
Util.prototype.hasDescElement = function(desc_list) {
    for(var i=0; i<desc_list.length; ++i) {
        if(desc(desc_list[i]).findOnce() == null)
            return false
    }
    return true
}

/**
 * @description: 获取屏幕上存在于 id 列表中的第一个控件
 * @param {ArrayList} id_list: id 列表
 * @return {widget}: 控件
 */
Util.prototype.widgetInIdList = function (id_list) {
    for(var i=0; i<id_list.length; ++i) {
        let widget = id(id_list[i]).findOnce()
        if(widget)  return widget
    }
    return null
}

/**
 * @description: 检查元素是否在列表中
 * @param {*} elem: 元素
 * @param {ArrayList} list: 列表
 * @return {Boolean}
 */    
Util.prototype.inList = function(elem, list) {
    for(var i=0; i<list.length; ++i) {
        if(elem == list[i]) return true
    }
    return false
},

/**
 * @description: 判断两个字典是否相同
 * @param {Map} dict_1: 字典一
 * @param {Map} dict_2: 字典二
 * @return {Boolean}
 */
Util.prototype.isSameDict = function(dict_1, dict_2) {
    for(var key in dict_1) {
        if(dict_1[key] != dict_2[key]) return false
    }
    for(var key in dict_2) {
        if(dict_2[key] != dict_1[key]) return false
    }
    return true
},

/**
 * @description: 检查给定字典是否在列表中
 * @param {Map} dict: 给定字典
 * @param {ArrayList} dict_list: 字典列表
 * @return {Boolean}
 */
Util.prototype.inDictList = function(dict, dict_list) {
    for(var i=0; i<dict_list.length; ++i) {
        if(this.isSameDict(dict, dict_list[i])) return i
    }
    return -1
}

/**
 * @description: 将第 idx 个元素移至列表末尾
 * @param {Integer} idx: 元素下标
 * @param {ArrayList} list： 列表
 * @return {ArrayList}: 移动后的列表
 */
Util.prototype.toListEnd = function(idx, list) {
    let tmp = null
    if(idx < list.length - 1)   tmp = list[idx]
    else return list

    for(var i=idx; i<list.length-1; ++i) {
        list[i] = list[i + 1]
    }
    list[list.length - 1] = tmp
    return list
}

Util.prototype.reverseList = function(list) {
    let i = 0
    let j = list.length - 1
    let tmp = null

    while(i<j) {
        tmp = list[i]
        list[i++] = list[j]
        list[j--] = tmp
    }
    return list
}

Util.prototype.copyDict = function(dict) {
    let target = {}
    for(var key in dict) {
        target[key] = dict[key]
    }
    return target
}

Util.prototype.isEmpty = function(elem) {
    if(elem == null || elem == undefined || elem == "" || elem == [])
        return true
    else
        return false
}

Util.prototype.stop = function () {
    this.debugInfo("退出自动记账")
    toast("退出自动记账")
    exit()
}

module.exports = new Util()