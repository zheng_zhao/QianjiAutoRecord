
/*
 * @Date         : 2021-02-16 22:26:35
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 17:35:42
 * @FilePath     : \QianjiAutoRecord\resources\defaultConfig\DefaultEnable.js
 */

var DefaultEnable = {
    "微信发红包": true, "微信收红包": true,     // 微信红包
    "微信发转账": true, "微信收转账": true,     // 微信转账
    "微信指纹支付": true, "微信密码支付": true, // 微信主动支付
    "微信付款码": true,                        // 微信付款码
    "支付宝付款框": true,                      // 支付宝主动支付
    "支付宝付款码": true,                      // 支付宝付款码
    "淘宝付款框": true,
    "美团订单页": true,
}

module.exports = DefaultEnable