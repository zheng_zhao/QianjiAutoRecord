
/*
 * @Date         : 2021-01-27 22:15:33
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-18 17:43:21
 * @FilePath     : \QianjiAutoRecord\resources\pageDict\PageDict.js
 */

const wechatPageDict = require("./WechatPageDict.js")
const aliPayPageDict = require("./AliPayPageDict.js")
const taobaoPageDict = require("./TaobaoPageDict.js")
const meituanPageDict = require("./MeituanPageDict.js")

let PageDict = function () {
    this.pageDict = {}

    this._loadDict(wechatPageDict)
    this._loadDict(aliPayPageDict)
    this._loadDict(taobaoPageDict)
    this._loadDict(meituanPageDict)
}

PageDict.prototype._loadDict = function(dict) {
    for(var key in dict) {
        this.pageDict[key] = dict[key]
    }
}

module.exports = (new PageDict()).pageDict