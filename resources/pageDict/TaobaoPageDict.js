
/*
 * @Date         : 2021-01-27 22:15:33
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-14 00:54:33
 * @FilePath     : \QianjiAutoRecord\resources\TaobaoPageDict.js
 */

var TaobaoPageDict = {
    "淘宝": {
        "default_page": true,
        "package_name": "com.taobao.taobao"
    },
    "淘宝付款框": {
        "package_name": "com.taobao.taobao",
        "activity": "com.alipay.android.msp.ui.views.MspContainerActivity",
        "text_list": ["支付宝账号", "付款方式"]
    },
    "淘宝付款框1": {
        "package_name": "com.taobao.taobao",
        "activity": "android.widget.LinearLayout",
        "text_list": ["支付宝账号", "付款方式"]
    },
    "淘宝付款框2": {
        "package_name": "com.taobao.taobao",
        "activity": "android.app.Dialog",
        "text_list": ["支付宝账号", "付款方式"]
    },
    "淘宝指纹支付": {
        "package_name": "com.taobao.taobao",
        "activity": "com.alipay.android.phone.seauthenticator.iotauth.fingerprint.FpFullViewCompatDialog",
    },
    "淘宝指纹支付1": {
        "package_name": "com.taobao.taobao",
        "activity": "android.widget.LinearLayout",
        "text_list": ["请验证指纹"]
    },
    "淘宝密码支付": {
        "package_name": "com.taobao.taobao",
        "activity": "com.alipay.mobile.verifyidentity.module.password.pay.ui.PayPwdDialogActivity",
    },
    "淘宝密码支付1": {
        "package_name": "com.taobao.taobao",
        "activity": "android.widget.LinearLayout",
        "text_list": ["请输入支付密码"]
    },
    "淘宝支付成功": {
        "package_name": "com.taobao.taobao",
        "activity": "android.widget.LinearLayout",
        "text_list": ["支付成功"]
    },
    "淘宝支付成功1": {
        "package_name": "com.taobao.taobao",
        "activity": "com.taobao.weex.WXActivity",
        "desc_list": ["支付成功"]
    },
    "淘宝选择付款方式": {
        "package_name": "com.taobao.taobao",
        "activity": "com.alipay.android.msp.ui.views.MspContainerActivity",
        "text_list": ["选择付款方式"]
    }
}

module.exports = TaobaoPageDict