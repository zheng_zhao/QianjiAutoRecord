
/*
 * @Date         : 2021-02-16 21:00:40
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-17 13:40:17
 * @FilePath     : \QianjiAutoRecord\monitor\WechatMonitor.js
 */

// const myUtil = require("../utils/Util.js")

var WechatMonitor = {}

WechatMonitor.activePayWechat = function (activity_monitor) {
    let related_page = ["微信指纹支付", "微信密码支付", "微信支付成功", "微信选择付款方式", "微信选择优惠"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let target = ""
    let remark = ""
    
    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true) {
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]
        
        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if (myUtil.inPageList(curr_page, ["微信密码支付", "微信指纹支付"])) {              // 更新金额和支付方式
            let pay_type_widget = text("支付方式").findOnce()
            if(pay_type_widget != null) {
                let parent_widget = myUtil.widgetByParentChild(pay_type_widget, 3, [])
                if(parent_widget) {
                    let _target = myUtil.widgetByParentChild(parent_widget, 0, [1, 0])
                    if(_target) target = _target.text().replace("付款给", "")
                    let _money = myUtil.widgetByParentChild(parent_widget, 0, [2, 0])
                    if(_money) money = _money.text()
                    money = money.substr(1, money.length)

                    let _account = myUtil.widgetByParentChild(parent_widget, 0, [4, 1, 1, 0])
                    if(!_account)   _account = myUtil.widgetByParentChild(pay_type_widget, 2, [1, 1, 0])
                    if(_account) account = _account.text()
                } 
            }
        } else if(myUtil.inPageList(curr_page, ["微信支付成功"])) {
            if(target)  remark = "付款给" + target
            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(timeCnt < 70) {
            let preview_first = myUtil.findPreviewFirstInPageList(["微信支付成功", "微信密码支付", "微信指纹支付"], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(!myUtil.inPageList(preview_first, ["微信支付成功"])) {     // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        }
        sleep(100)
    }
}

WechatMonitor.passivePayWechat = function (activity_monitor) {
    let related_page = ["微信付款码", "微信密码支付", "微信指纹支付", "微信支付成功", "微信选择付款方式",
                        "微信大付款码", "微信支付加载"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["微信付款码"])) {
            let _account = id("com.tencent.mm:id/jie").findOnce()
            if(_account) account = _account.text()
        } else if(myUtil.inPageList(curr_page, ["微信支付成功"])) {
            let _money = myUtil.widgetInIdList(["com.tencent.mm:id/iu8"])
            if(_money)  money = _money.text()

            let _target = myUtil.widgetInIdList(["com.tencent.mm:id/ac8"])
            if(_target) target = _target.text()
            
            if(target)                          remark = "向" + target + "付款"
            if(!myUtil.isEmpty(description)){
                if(!myUtil.isEmpty(remark))         remark = remark + "，"
                remark = remark + "订单为：" + description
            }
            
            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(timeCnt < 70) {
            let preview_first = myUtil.findPreviewFirstInPageList(["微信支付成功", "微信付款码",], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(!myUtil.inPageList(preview_first, ["微信支付成功"])) {     // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        }
        sleep(100)
    }
}

WechatMonitor.sendWechatRedBag = function (activity_monitor) {
    let related_page = ["微信发红包", "微信密码支付", "微信指纹支付", "微信验证指纹", "微信支付加载"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)   break
        
        if(curr_page == "微信发红包") {                         // 更新留言
            let des = myUtil.widgetInIdList(["com.tencent.mm:id/f4t"])
            if(des != null){
                description = des.text()
            } 
        } else if(myUtil.inPageList(curr_page, ["微信密码支付", "微信指纹支付"])) {              // 更新金额和支付方式
            let pay_type_widget = text("支付方式").findOnce()
            if(pay_type_widget != null) {
                let parent_widget = myUtil.widgetByParentChild(text("支付方式").findOnce(), 3, [])
                if(parent_widget) {
                    let _money = myUtil.widgetByParentChild(parent_widget, 0, [2, 0])
                    if(_money) money = _money.text()
                    money = money.substr(1, money.length)
                    
                    let _account = myUtil.widgetByParentChild(parent_widget, 0, [4, 1, 1, 0])
                    if(_account) account = _account.text()
                }
            }
        } else if(myUtil.inPageList(curr_page, ["微信聊天界面"])){
            let _target = myUtil.widgetInIdList(["com.tencent.mm:id/ipt"])
            if(_target)     target = _target.text()
            if(target)      remark = "给" + target + "发红包"
            if(!myUtil.isEmpty(description)) {
                if(!myUtil.isEmpty(remark))     remark = remark + "，"
                remark = remark + "留言为：" + description
            }
            let preview_first = myUtil.findPreviewFirstInPageList(["微信发红包", "微信指纹支付", "微信密码支付"], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(myUtil.inPageList(preview_first, ["微信指纹支付", "微信密码支付"])) {
                activity_monitor._save(type, money, account, remark, target)
                break
            } else {                                           // 未支付切出 取消
                activity_monitor.debugInfo("未支付切出")
                activity_monitor._cancel()
                break
            }
        } else {
            // 输入描述时，无法确定页面
            let des = myUtil.widgetInIdList(["com.tencent.mm:id/f4t"])
            if(des != null){
                description = des.text()
                timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100
            }
        }
        sleep(100)
    }
}

WechatMonitor.receiveWechatRedBag = function (activity_monitor) {
    let related_page = ["微信收红包", "微信红包详情"]
    let type = "收入"
    let money = 0.00
    let account = "零钱"
    let description = ""
    let target = ""
    let remark = ""
    let success = false

    
    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true) {
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["微信红包详情"])) {
            if(textEndsWith("等待对方领取").findOnce())  {
                toast("该红包已发出")
                activity_monitor.debugInfo("红包发出人点进详情")
                activity_monitor._cancel()
                break
            }
            let _target = textEndsWith("的红包").findOnce()
            if(_target)    target = _target.text()
            target = target.replace("的红包", "")

            let _des = id("com.tencent.mm:id/f06").findOnce()
            if(_des)    description = _des.text()

            let _money = id("com.tencent.mm:id/eyq").findOnce()
            if(_money)  money = _money.text()

            if(target)                          remark = "收取 " + target + "的红包"
            if(!myUtil.isEmpty(description)) {
                if(!myUtil.isEmpty(remark))     remark = remark + "，"
                remark = remark + "留言为：" + description
            }

            activity_monitor._save(type, money, account, remark, target)
            break
        } else if(myUtil.inPageList(curr_page, ["微信聊天界面"])) {
            let preview_first = myUtil.findPreviewFirstInPageList(["微信收红包", "微信红包详情"], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            if(myUtil.inPageList(preview_first, ["微信收红包"])) {  // 未领取切出
                activity_monitor.debugInfo("未领取切出")
                activity_monitor._cancel()
                break
            }
        } else if(timeCnt < 70) {
            activity_monitor.debugInfo("未领取切出")
            activity_monitor._cancel()
            break
        }
        sleep(100)
    }
}

WechatMonitor.transInWechat = function (activity_monitor) {
    let related_page = ["微信收转账", "微信转账收取确认"]
    let type = "收入"
    let money = 0.00
    let account = "零钱"
    let description = ""
    let target = ""
    let remark = ""
    let success = false

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true) {
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, ["微信转账收取确认"])) {
            let preview_first = myUtil.findPreviewFirstInPageList(["微信收转账", "微信聊天界面"], activity_monitor.pageRecord, activity_monitor.pageRecordIdx)
            // 支付成功
            if(myUtil.inPageList(preview_first, ["微信收转账"])) {
                let des = myUtil.widgetByParentChild(text("转账说明").findOnce(), 1, [1])
                if(des) description = des.text()

                let _money = myUtil.widgetByParentChild(text("你已收款").findOnce(), 1, [2])
                if(_money) _money = _money.text()
                if(_money) money = _money.substr(1, _money.length)

                if(!success)    toast("转账收取成功，请后退至聊天页，获取转账对象！")
                success = true
            } else {                                           // 之前领取的转账
                activity_monitor.debugInfo("之前领取的转账，不记录")
                activity_monitor._cancel()
                break
            }
        } else if(myUtil.inPageList(curr_page, ["微信聊天界面"])) {
            // 支付成功
            if(success) {
                let _target = myUtil.widgetInIdList(["com.tencent.mm:id/ipt"])
                if(_target) target = _target.text()
                if(target)                          remark = "收取" + target + "的转账"
                if(!myUtil.isEmpty(description)) {
                    if(!myUtil.isEmpty(remark))     remark = remark + "，"
                    remark = remark + "留言为：" + description
                }

                activity_monitor._save(type, money, account, remark, target)
                break
            } else {                                // 未领取切出
                activity_monitor.debugInfo("未领取切出")
                activity_monitor._cancel()
                break
            }
        } else if(success){    // 支付成功
            if(timeCnt < 70) {
                remark = "收取转账"
                if(!myUtil.isEmpty(description))
                    remark = remark + "，留言为：" + description
                
                activity_monitor._save(type, money, account, remark, target)
                break
            }             
        }
        sleep(100)
    }
}

WechatMonitor.transOutWechat = function (activity_monitor) {
    let related_page = ["微信发转账", "微信添加转账说明", "微信密码支付",
                        "微信指纹支付",  "微信支付成功", "微信验证指纹", "微信支付加载"]
    let type = "支出"
    let money = 0.00
    let account = ""
    let description = ""
    let target = ""
    let remark = ""
    let success = false

    let timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100

    while(true){
        let curr_page = activity_monitor.pageRecord[activity_monitor.pageRecordIdx % activity_monitor.pageRecord.length]

        timeCnt = activity_monitor._preprocess(curr_page, related_page, timeCnt)
        if(timeCnt == 0)    break

        if(myUtil.inPageList(curr_page, "微信发转账")) {                         // 更新留言
                let des = myUtil.widgetInIdList(["com.tencent.mm:id/h13"])
            if(des != null){
                description = des.text()
                description = description.substr(0, description.length - 3)
            }
        } else if (myUtil.inPageList(curr_page, ["微信密码支付", "微信指纹支付"])) {              // 更新金额和支付方式
            let pay_type_widget = text("支付方式").findOnce()
            if(pay_type_widget != null) {
                let parent_widget = myUtil.widgetByParentChild(text("支付方式").findOnce(), 3, [])
                if(parent_widget) {
                    let _money = myUtil.widgetByParentChild(parent_widget, 0, [2, 0])
                    if(_money) money = _money.text()
                    money = money.substr(1, money.length)

                    let _account = myUtil.widgetByParentChild(parent_widget, 0, [4, 1, 1, 0])
                    if(_account) account = _account.text()
                } 
            }
        } else if (myUtil.inPageList(curr_page, ["微信支付成功"])) {
            if(!success) {
                let widget = textEndsWith("确认收款").findOnce()
                if(widget) {
                    target = widget.text()
                    target = target.replace("待", "").replace("确认收款", "")
                    remark = "给" + target + " 转账"
                }
                if(!myUtil.isEmpty(description)) {
                    if(!myUtil.isEmpty(remark)) remark = remark + "，"
                    remark = remark + "留言为：" + description
                }
                
                activity_monitor._save(type, money, account, remark, target)
                break
            }
        }else {
            // 输入描述时，无法确定页面
            let widget = id("com.tencent.mm:id/bxz").findOnce()
            if(widget) {
                description = widget.text()
            timeCnt = activity_monitor.unrelatedPageTimeOutSecond / 100
            }
        }
        sleep(100)
    }
}

module.exports = WechatMonitor