
/*
 * @Date         : 2021-02-17 13:09:20
 * @LastEditors  : LiZhenglin
 * @LastEditTime : 2021-02-17 13:42:09
 * @FilePath     : \QianjiAutoRecord\AutoRecord.js
 */

// const myUtil = require("./utils/Util.js")
const record = require("./Record.js")
const activityMonitor = require("./monitor/ActivityMonitor.js")

const mainPage = require("./layout/MainPage.js")
const bookConfigPage = require("./layout/BookConfigPage.js")
const accountConfigPage = require("./layout/AccountConfigPage.js")
const bookConfigDetailPage = require("./layout/BookConfigDetailPage.js")

const default_books = require("./resources/defaultConfig/DefaultBooks.js")
const default_enable = require("./resources/defaultConfig/DefaultEnable.js")
const default_account = require("./resources/defaultConfig/DefaultAccount.js")

let AutoRecord = function() {
    this.storage = storages.create("钱迹自动记账数据库")
    this.default_books = default_books
    
    this.MainPage = mainPage
    this.BookConfigPage = bookConfigPage
    this.AccountConfigPage = accountConfigPage
    this.BookConfigDetailPage = bookConfigDetailPage
    
    // 初始化配置
    this.init()
    // 加载数据库
    this.load()
}

AutoRecord.prototype.run = function () {
    // 开始运行
    this.run_flag = false
    this.MainPage(this)
    // this.startRecord()
}

AutoRecord.prototype.init = function() {
    this.global_config = this.storage.get("GlobalConfig", {})
    this.account_config = this.storage.get("AccountsConfig", null)
    this.book_config = this.storage.get("BooksConfig", null)
    // 备注配置
    if(!this.global_config["remark_prefix"]) this.global_config["remark_prefix"] = ""
    if(!this.global_config["remark_suffix"]) this.global_config["remark_suffix"] = ""
    if(this.global_config["last_remark"] == null || this.global_config["last_remark"] == undefined)
        this.global_config["last_remark"] = false
    // 资产账户启用
    if(this.global_config["has_book"] == null || this.global_config["has_book"] == undefined)
        this.global_config["has_book"] = true
    // 多账本启用
    if(this.global_config["has_account"] == null || this.global_config["has_account"] == undefined)
        this.global_config["has_account"] = true
    // 启用特性
    if(!this.global_config["enable_feature"]) this.global_config["enable_feature"] = default_enable
    // 保存全局配置
    this.storage.put("GlobalConfig", this.global_config)

    // 账户配置
    if(this.account_config == undefined || this.account_config == null || this.account_config == [])
        this.account_config = default_account
    // 保存账户配置
    this.storage.put("AccountsConfig", this.account_config)

    // 账本配置
    if(!this.book_config || this.book_config == []) this.book_config = [ myUtil.copyDict(default_books[0]) ]
    // 保存账本配置
    this.storage.put("BooksConfig", this.book_config)
}

AutoRecord.prototype.load = function() {
    this.global_config = this.storage.get("GlobalConfig", {})
    this.remark_prefix = this.global_config["remark_prefix"]
    this.remark_suffix = this.global_config["remark_suffix"]
    this.last_remark = this.global_config["last_remark"]
    this.has_account = this.global_config["has_account"]
    this.has_book = this.global_config["has_book"]
    this.enable_feature = this.global_config["enable_feature"]
}

AutoRecord.prototype.startRecord = function() {
    if(this.run_flag)    { toast("已运行请勿重复运行"); return false }
    this.run_flag = true
    // 构建启用特性列表
    let enable_feature_list = []
    for(var key in this.enable_feature) {
        if(this.enable_feature[key]) {
            enable_feature_list.push(key)
        }
    }
    // 构建账户列表
    let accounts_list = []
    for(var i=0; i<this.account_config.length; ++i) {
        let from_account = this.account_config[i]["from_account"]
        let to_account = this.account_config[i]["to_account"]
        let item = {}
        item[from_account] = to_account
        accounts_list.push(item)
    }
    
    // 构建账本列表
    let books_dict = {}
    for(var i=0; i<this.book_config.length; ++i) {
        let book_name = this.book_config[i]["book_name"]
        let enable = this.book_config[i]["enable"]
        let income_list = this.book_config[i]["收入"]
        let outcome_list = this.book_config[i]["支出"]
        books_dict[book_name] = { "enable": enable, "收入": income_list, "支出": outcome_list}
    }

    // 初始化记录器
    record.init(books_dict, accounts_list, this.global_config)
    // 初始化监控器
    activityMonitor.init(this.global_config, record)
    // 开始监控
    activityMonitor.monitor(enable_feature_list)
    // record.construct("支出", "1111.11", "零钱", "测试备注")
    // record.record()
}

AutoRecord.prototype.SaveBookConfig = function() {
    this.storage.put("BooksConfig", this.book_config)
}

AutoRecord.prototype.saveAccountConfig = function() {
    this.storage.put("AccountsConfig", this.account_config)
}

AutoRecord.prototype.saveGlobalConfig = function() {
    this.remark_prefix = this.remark_prefix.toString()
    this.remark_suffix = this.remark_suffix.toString()

    this.global_config["remark_prefix"] = this.remark_prefix.toString()
    this.global_config["remark_suffix"] = this.remark_suffix.toString()
    this.global_config["last_remark"] = this.last_remark
    this.global_config["has_account"] = this.has_account
    this.global_config["has_book"] = this.has_book
    this.storage.put("GlobalConfig", this.global_config)
}

AutoRecord.prototype.saveEnableFeatureConfig = function() {
    this.global_config["enable_feature"] = this.enable_feature
    this.storage.put("GlobalConfig", this.global_config)
}

module.exports = new AutoRecord()