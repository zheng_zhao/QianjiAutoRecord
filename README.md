# 钱迹自动记账

钱迹自动记账项目，使用 Auto.js 开发。

![测试发红包](readme_img/测试发红包.gif)  ![测试收红包](readme_img/测试收红包.gif)

考虑到使用的便捷性，已打包成 apk。

使用无障碍监控分析屏幕，在各种付款场景完成后弹出悬浮窗。其中，**金额、账户**自动分析得出，**类别、账本**根据该收款对象上一次的记账记录自动填充。同时列出历史“账本|类别”组合及历史备注。

首次使用前，需要根据自己的钱迹修改配置文件。

项目地址：https://gitee.com/dora-cmon/QianjiAutoRecord

QQ 交流群：1016266004 （问题答案：dora_cmon)

**支持场景：**

- 微信（收/发红包、收/发转账、扫码支付、付款码支付）
- 支付宝（发转账、扫码支付、付款码支付）
- 淘宝
- 美团

## 一、环境说明

**必备软件：**
- 钱迹

**测试环境：**
- 小米9 / MIUI 12.5 开发版 
- Android 11
- 微信 8.0.0 (不支持低版本微信)
- 支付宝 10.2.13

## 二、使用说明

### 1. 赋予权限
   
![赋予权限](readme_img/赋予权限.png)

- 自启动
- 省电策略：不限制
- 无障碍服务
- 后台锁定

### 2. 系统设置

打开应用，禁用“音量上键关闭所有脚本”，启用“前台服务”。

![系统设置](readme_img/系统设置.png)

### 3. 配置账户、账本

![配置账户账本](readme_img/配置账户账本.png)

图（1）中，“资产账户”对应钱迹实验室中的资产账户功能，“多账本”为钱迹的会员功能。根据钱迹的情况，开启/关闭这两个功能。

- **账户配置**

    注意：完成配置后点击“保存”按钮，否则修改不生效。

    图（2）左侧为各 App 付款界面显示的账户名称，右侧为钱迹中的账户名称。

    记账时，会将页面上检测到的账户名称，映射到钱迹中的账户名称。（例如，支付宝中付款账户为“中国工商银行借记卡(0000)，而钱迹中该账户为“工商银行借记卡(0000)"）

- **账本配置**

    注意：完成配置后点击“保存”按钮，否则修改不生效。

    图（3）上方为钱迹中的账本模板，点击可直接新建账本。下方为你的账本列表，点击可进入详情页。

    图（4）账本详情页，可对该账本下的类别增删改，修改某条数据需点击“改”按钮。


## 三、源码运行说明

运行源码参考本节（直接安装应用的，跳过本节）

## 特性

1. 进入记账流程后，右上角显示圆形图标，点击悬浮窗显示红色关闭按钮，点击关闭按钮可以可终止程序（彻底关闭），10s不关闭会返回蓝色图标：部分界面没有悬浮窗权限，如 `支付宝付款框, 微信指纹支付, 微信密码支付` 界面，不显示图标，但会提示检测到该页面。

2. 显示记账悬浮窗后，点击左上角圆形倒计时，关闭悬浮窗，不记账；倒计时结束或点击 `$` 图标，按照当前选择自动记账。

3. 显示记账悬浮窗后，点击账本、类别、账户，下方会显示可选择项。左右滑动选择项可进行翻页，上方的左右指示变蓝说明可以向该方向翻页。

4. 部分页面完成支付后无悬浮窗权限，需要退出该页面才可继续记账。

5. 熄屏后，即使倒计时完成也不记账。

6. 根据上一次付给收款人时填写的信息自动填充类别、账本，并在下方显示该收款人历史“账本|账户”组合
   
7. 点击备注，显示该收款人的历史备注信息（第一个为本次识别的备注信息）

## 四、版本更新

### V_1.3.1

1. 重构代码
2. 适配淘宝、美团
3. 已收红包不记录
4. 适配支付宝发转账
5. 优化退出记账流程的检测
6. 修复微信/支付宝付款码偶尔失败
7. 将“资产账户”及“多账本“融合进主程序
8. 将“音量上键关闭脚本”及“前台服务”的设置融合进主程序

## To do （以后再做）

1. 支付宝发红包
2. 适配京东
3. 适配饿了么
   
## 常见问题

1. 识别账户为默认账户

    检查账户配置中，是否已配置当前付款账户，并检查账户名是否与付款界面显示的完全一致；

2. 微信不记账

    微信旧版本不适配，必须为 8.0 及以上